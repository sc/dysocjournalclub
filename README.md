![DySoc Logo](http://www.dysoc.org/images/dysocbanner.png)


# DySoC Journal Club

This reporitory simply list potential papers to be discussed during DySoC journal club and the schedule of the DySoC Journal Club.  The pages can be found: dysoc.org/~simon/dysocjc


To render and deploy it (with Rmardown):

```r
library(bookdown)
render_book(input="dysocjc.Rmd",output_dir="~/public_html/dysocjc/", output_file="~/public_html/dysocjc/index.html")
```

